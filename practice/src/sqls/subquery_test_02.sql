/*
 * 请告诉我所有订单（order）中的订单明细的最大数目、最小数目和平均数目。结果应当包含三列：
 *
 * +────────────────────+────────────────────+────────────────────+
 * | minOrderItemCount  | maxOrderItemCount  | avgOrderItemCount  |
 * +────────────────────+────────────────────+────────────────────+
 */
SELECT min(OrderItemCount) AS minOrderItemCount,
       max(OrderItemCount) AS maxOrderItemCount,
       ROUND(avg(OrderItemCount))
                           AS avgOrderItemCount
FROM (SELECT count(orderNumber) AS OrderItemCount FROM orderdetails GROUP BY orderNumber) AS orderItem;
