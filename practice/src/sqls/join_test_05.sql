/*
 * 请告诉我所有员工（employee）中有管理者（manager）的员工姓名及其管理者的姓名。所有的姓名
 * 请按照 `lastName, firstName` 的格式输出。例如：`Bow, Anthony`：
 *
 * +───────────+──────────+
 * | employee  | manager  |
 * +───────────+──────────+
 *
 * 输出结果按照 `manager` 排序，然后按照 `employee` 排序。
 */
SELECT concat(e.lastName, ', ', e.firstName) AS employee, concat(m.lastName, ', ', m.firstName) AS manager
FROM employees e
         INNER JOIN employees m
                    ON e.reportsTo = m.employeeNumber
WHERE e.reportsTo IS NOT NULL
ORDER BY manager, employee;
